package main

import (
	"bufio"
	"fmt"
	"net"
	"os/exec"
	"strings"
)

func main() {
	startServer()
}

func startServer() {
	fmt.Println("Starting server...")

	listener, err := net.Listen("tcp", ":3117")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer listener.Close()

	accept(listener)

}

func accept(listener net.Listener) {
	conn, err := listener.Accept()
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		netData, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			fmt.Println(err)
			accept(listener)
		}
		message := strings.TrimSpace(string(netData))
		switch message {
		case "STOP":
			fmt.Println("Exiting TCP server!")
			return
		case "toggleMute":
			fmt.Println("Toggling Zoom mute")
			cmd := exec.Command("/bin/sh", "/home/laszlokocsonya/go/src/zoomController/toggleZoomMute.sh")
			err := cmd.Run()
			if err != nil {
				panic(err)
			}
		}

		fmt.Print("-> ", string(netData))
	}
}
