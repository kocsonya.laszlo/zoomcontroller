# Zoom mute controller
## Installation
- Clone the repo
- go build main.go
- sudo mkdir /opt/zoomController
- sudo cp main toggleZoomMute.sh /opt/zoomController
- sudo cp zoomController.service /etc/systemd/system
- sudo service zoomController start

## Sending message to the server 
```bash
echo toggleMute | ncat --send-only 192.168.2.10 3117
```